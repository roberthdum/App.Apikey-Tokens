﻿namespace App.Tokens
{
    public interface ITokenService
    {
        string GenerateToken(string apiKey, string apiSecret, string issuer, string audience, int expirationMinutes);
    }
}