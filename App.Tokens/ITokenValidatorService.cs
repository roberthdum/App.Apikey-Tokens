﻿namespace App.Tokens
{
    public interface ITokenValidatorService
    {
        bool ValidateToken(string token, string apiSecret, string issuer= "", string audience = "");
    }
}