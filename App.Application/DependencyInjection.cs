﻿
using App.Domain.repository;
using App.Infra;
using App.Tokens;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Reflection;

namespace App.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)        {

            services.AddSingleton<ITokenValidatorService, TokenValidatorService>();
            services.AddSingleton<ITokenService, TokenService>();
            services.AddScoped<ApiContext>();
            services.AddScoped<IApikeyRepository, ApikeyRepository>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}