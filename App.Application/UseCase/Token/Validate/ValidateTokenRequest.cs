﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Application.UseCase.Token.Validate
{
    internal class ValidateTokenRequest
    {
        public record struct ValidateTokenResponse(string token) { }
    }
}
