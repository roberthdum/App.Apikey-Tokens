﻿
using App.Apikey_Tokens.UseCase.Token.Create;
using App.Domain.Entities;
using App.Domain.repository;
using App.Infra;
using App.Tokens;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;

namespace App.Application.UseCase.Token.Validate
{
    public class ValidateTokenCommand : IRequest<Response<ValidateTokenResponse>>
    {
        public string Token { get; set; }

    }
    public class ValidateTokenCommandHandler : IRequestHandler<ValidateTokenCommand, Response<ValidateTokenResponse>>
    {
        private readonly ILogger<ValidateTokenCommandHandler> _logger;
        private IApikeyRepository _repository;

        private IConfiguration _configuration { get; }
        private readonly ITokenValidatorService _tokenValidator;
        public ValidateTokenCommandHandler(ILogger<ValidateTokenCommandHandler> logger, ITokenValidatorService tokenValidator, IConfiguration configuration, IApikeyRepository repository)
        {
            _tokenValidator = tokenValidator;
            _logger = logger;
            _configuration = configuration;
            _repository = repository;
        }

        public async Task<Response<ValidateTokenResponse>> Handle(ValidateTokenCommand request, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Entro");
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(request.Token);
            var tokenS = handler.ReadToken(request.Token) as JwtSecurityToken;
            Console.WriteLine("Subject: " + tokenS.Subject);
            Console.WriteLine("Issuer: " + tokenS.Issuer);
            Console.WriteLine("Expires: " + tokenS.ValidTo);
            Console.WriteLine("Claims: ");
            foreach (var claim in tokenS.Claims)
            {
                Console.WriteLine("Type: " + claim.Type + " Value: " + claim.Value);
            }

            var api = await _repository.GetByApikey(tokenS.Claims.FirstOrDefault().Value);
            if (api == null)
            {
                return new Response<ValidateTokenResponse>
                {
                    Content = new ValidateTokenResponse
                    {
                        isvalid = false

                    },
                    StatusCode = System.Net.HttpStatusCode.Unauthorized
                };
            }
            var isvalid = _tokenValidator.ValidateToken(request.Token, _configuration.GetValue<string>("IssuerSigningKey"), api.ApiName, "Internal");
            var statusCode = (!isvalid ? System.Net.HttpStatusCode.Unauthorized : System.Net.HttpStatusCode.Accepted);
            return new Response<ValidateTokenResponse>
            {
                Content = new ValidateTokenResponse
                {
                    isvalid = isvalid,
                    messague = "--lo que sea-**",

                },
                StatusCode = statusCode
            };
        }
    }

}