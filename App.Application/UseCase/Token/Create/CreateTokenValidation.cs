﻿using FluentValidation;
using static App.Apikey_Tokens.UseCase.Token.Create.CreateTokenValidation;

namespace App.Apikey_Tokens.UseCase.Token.Create
{
    public class CreateTokenValidation : AbstractValidator<CreateTokenCommand>
    {
        public CreateTokenValidation()
        {
            RuleFor(x => x.Apikey)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Apikey is invalid")
                .MinimumLength(10).WithMessage("no puede tener menos de 50 caracteres")
                .NotNull().WithMessage("NO puede estar null");

        }
    }
}
