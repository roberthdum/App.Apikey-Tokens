﻿using static App.Apikey_Tokens.UseCase.Token.Create.CreateTokenRequest;
using App.Infra;
using App.Tokens;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using System;
using Microsoft.Extensions.Configuration;
using App.Domain.repository;
using System.Web.Mvc;

namespace App.Apikey_Tokens.UseCase.Token.Create
{
    public class CreateTokenCommand : IRequest<Response<CreateTokenResponse>>
    { 
        public string Apikey { get; set; }
        public string Secreet { get; set; }

}

    public class CreateTokenCommandHandler : IRequestHandler<CreateTokenCommand, Response<CreateTokenResponse>>
    {
        private readonly ILogger<CreateTokenCommandHandler> _logger;
        private IApikeyRepository _repository;
        private IConfiguration _configuration { get; }
        private readonly ITokenService _tokenGererator;
        public CreateTokenCommandHandler( ILogger<CreateTokenCommandHandler> logger, ITokenService tokenGererator, IConfiguration configuration, IApikeyRepository repository)
        {
            _tokenGererator = tokenGererator;
              _logger = logger;
            _configuration = configuration;
            _repository= repository;
        }

        public async Task<Response<CreateTokenResponse>> Handle(CreateTokenCommand request, CancellationToken cancellationToken)        
        {
           
            _logger.LogDebug("Entro");

            var api = await _repository.GetBySecreetAndApikey(request.Apikey, request.Secreet);
            if(api == null )
            {
                return new Response<CreateTokenResponse>
                {
                    Content = new CreateTokenResponse
                    {
                        Token = ""

                    },
                    StatusCode = System.Net.HttpStatusCode.Unauthorized
                };
            }
            return new Response<CreateTokenResponse>
            {
                Content = new CreateTokenResponse
                {
                    Token = _tokenGererator.GenerateToken(request.Apikey, _configuration.GetValue<string>("IssuerSigningKey"), api.ApiName, "Internal", _configuration.GetValue<int>("expiration"))

                },
                StatusCode = System.Net.HttpStatusCode.Created
            };
        }
    }
}
