﻿using App.Domain.Entities;

namespace App.Domain.repository
{
    public interface IApikeyRepository
    {
        Task Add(ApiKey apikey);
        Task Delete(ApiKey apikey);
        Task<List<ApiKey>> GetAll();
        Task<ApiKey> GetByApikey(string apikey);
        Task<ApiKey> GetBySecreetAndApikey(string apikey, string secreet);
        Task Update(ApiKey apikey);
    }
}