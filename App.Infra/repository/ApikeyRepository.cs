﻿using App.Domain.Entities;
using App.Infra;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.repository
{
    public class ApikeyRepository : IApikeyRepository
    {
        private readonly ApiContext _context;

        public ApikeyRepository(ApiContext context)
        {
            _context = context;
        }

        public async Task<List<ApiKey>> GetAll()
        {
            return _context.Apikeys.ToList();
        }

        public async Task<ApiKey> GetBySecreetAndApikey(string apikey, string secreet)
        {
            return await _context.Apikeys.SingleOrDefaultAsync(x => x.Secreet == secreet && x.Apikey == apikey);
        }
    public async Task<ApiKey> GetByApikey(string apikey)
        {
            return await _context.Apikeys.SingleOrDefaultAsync(x => x.Apikey == apikey);
        }
        public async Task Add(ApiKey apikey)
        {
            _context.Apikeys.Add(apikey);
            await _context.SaveChangesAsync();
        }

        public async Task Update(ApiKey apikey)
        {
            _context.Apikeys.Update(apikey);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(ApiKey apikey)
        {
            _context.Apikeys.Remove(apikey);
            await _context.SaveChangesAsync();
        }
    }
}
