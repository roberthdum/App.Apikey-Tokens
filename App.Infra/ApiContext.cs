﻿using App.Domain.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

using System.Net;


namespace App.Infra
{

    public class ApiContext : DbContext    {
     

        public ApiContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {      

           SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder("Server=localhost;Database=testtoken;User Id=sa;Password=ratonera*1;");
            connectionStringBuilder.Encrypt = false;
            connectionStringBuilder.TrustServerCertificate = true;
            optionsBuilder.UseSqlServer(connectionStringBuilder.ConnectionString);

        }

   

        public DbSet<ApiKey> Apikeys { get; set; }
    }
 
}
