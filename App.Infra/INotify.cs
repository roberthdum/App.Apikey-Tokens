﻿namespace App.Infra
{
    public interface INotify
    {
        string Code { get; set; }
        string Message { get; set; }
        string Property { get; set; }

        string ToString();
    }
}