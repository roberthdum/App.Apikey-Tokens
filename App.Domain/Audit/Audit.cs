﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Audit
{
    public class Auditory
    {
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateUpdate { get; set; }
        public DateTime DateInserted { get; set; }


    }
}
