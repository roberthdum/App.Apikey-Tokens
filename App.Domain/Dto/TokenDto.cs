﻿using App.Domain.Entities;

public record struct TokenDto(int Id, string ApikeyId, string JwToken) { }
