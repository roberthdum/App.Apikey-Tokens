﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Domain.Audit;
namespace App.Domain.Entities
{
    public class Token : Auditory
    {
        public int Id { get; set; }
        public ApiKey ApikeyId { get; set; }
        public string JwToken { get; set; }

    }
}
