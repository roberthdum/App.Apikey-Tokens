﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Domain.Audit;
namespace App.Domain.Entities
{
    public class ApiKey: Auditory
    {
        public int Id { get; set; }
        public string Apikey { get; set; }
        public string Secreet { get; set; }
        public string ApiName { get; set; }


    }
}
