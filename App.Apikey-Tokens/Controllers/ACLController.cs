using App.Apikey_Tokens.UseCase.Token.Create;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using App.Infra;
using Microsoft.AspNetCore.Authorization;
using App.Application.UseCase.Token.Validate;

namespace App.Apikey_Tokens.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ACLController : ApiControllerBase
    {      

        private readonly ILogger<ACLController> _logger; 
        public ACLController(ILogger<ACLController> logger)
        {
            _logger = logger;
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Notify>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Notify>), StatusCodes.Status404NotFound)]
        [HttpPost("Token")]
        public async Task<IActionResult> Create(CreateTokenCommand body) => Result(await Mediator.Send(body));


        [HttpPost("ValidateToken")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Notify>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Notify>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Validate(ValidateTokenCommand body) => Result(await Mediator.Send(body));

        [HttpGet(Name = "TestToken")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<Notify>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Notify>), StatusCodes.Status404NotFound)]
        public IActionResult TestAutorization()
        {
            return Ok($"Request count: si tienes permiso");
        }


    }
}