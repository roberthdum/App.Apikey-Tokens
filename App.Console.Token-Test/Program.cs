﻿using System;
using System.Security.Cryptography;
using App.Tokens;
using Microsoft.Extensions.DependencyInjection;

var services = new ServiceCollection();
services.AddSingleton<ITokenService, TokenService>();
services.AddSingleton<ITokenValidatorService, TokenValidatorService>();
var serviceProvider = services.BuildServiceProvider();
var tokenClass = serviceProvider.GetService<ITokenService>();
var tokenValidation = serviceProvider.GetService<ITokenValidatorService>();
// Generate a token

            var apiKey ="00000000001";
            var apiSecret = Guid.NewGuid().ToString();
            var issuer = "api-01";
            var audience = "getapi";
            var expirationMinutes = 60;
            var token = tokenClass?.GenerateToken(apiKey, apiSecret, issuer, audience, expirationMinutes);
            Console.WriteLine("Token: " + token);

            // Validate the token
            var isValid = tokenValidation?.ValidateToken(token, apiSecret, issuer, audience);
            Console.WriteLine("Is valid: " + isValid);

Console.ReadLine();